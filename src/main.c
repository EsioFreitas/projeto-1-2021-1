#include <stdio.h>
#include <lcd_control.h>
#include <read_bme280.h>
#include <modbus.h>
#include <unistd.h>
#include <wiringPi.h>
#include <stdio.h>
#include <gpio.h>
#include <pid.h>
#include <utils.h>
#include <stdlib.h>
#include <signal.h>

#define KP 5.0
#define KI 1.0
#define KD 5.0
#define ADDRESS_BME280 0x76
#define CHANNEL_BME280 1

int ref_manual = 0;
double control_signal = 0.0;
float internal_temperature = 0.0;
float reference_temperature = 0.0;
int temperature_external = 0.0;
int option_histerese = 0;
int option_params = 0;
float ref_temp = 0.0;

void get_manual_reference(float *internalTemp, float *referenceTemp);
void menu();

void get_reference(float *internalTemp, float *referenceTemp)
{

  getInformationModbus(internalTemp, referenceTemp, ref_manual);

  getInformationBME280(&temperature_external);

  float temperatureExt = (float)temperature_external / 100;

  system("clear");
  printf("Digite a temperatura de referência desejada:\n");
  scanf("%f", &ref_temp);

  if (ref_temp < temperatureExt || ref_temp > 100)
  {
    system("clear");
    printf("A temperatura não pode ser menor que %.2f Cº e maior que 100 Cº\n", temperatureExt);
    sleep(2);
    menu(internalTemp, referenceTemp);
  }
  else
  {
    *referenceTemp = ref_temp;
    ref_manual = 1;
  }
}

void finish()
{
  turnOFFFAN();
  turnOFFResistor();
  closeUART();
  exit(0);
}

void sub_menu()
{
  char option;
  printf("1) On-Off (funcionalidade não desenvolvida)\n");
  printf("2) PID\n");
  printf("3) Chave externa (funcionalidade não desenvolvida)\n");

  option = getchar();

  switch (option)
  {
  case '1':
    printf("Deseja definir o valor de histerese? 1 - SIM ou 0 - NAO \n");
    scanf("%i", &option_histerese);
    if (option_histerese)
    {
      printf("Digite o valor da histerese: ");
      // TODO chamar on off passando histerese
    }
    else
    {
      // TODO chamar on off
    }
    break;

  case '2':
    printf("Deseja definir os parâmetros Kp, Ki, Kd? 1 - SIM ou 0 - NAO \n");
    scanf("%i", &option_params);
    if (option_params)
    {
      printf("Digite o valor do Kp: ");
      scanf("%f", &internal_temperature);
      printf("\n");
      printf("Digite o valor do Ki: ");
      scanf("%f", &reference_temperature);
      printf("\n");
      printf("Digite o valor do Kd: ");
      scanf("%f", &ref_temp);
      printf("\n");
    }
    else
    {
      ref_manual = 0;
    }
    break;

  case '3':
  default:
    system("clear");
    printf("Opção errada ou não desenvolvida, escolha novamente\n");
    sleep(1);
    sub_menu();
    break;
  }
}

void menu()
{
  char option;
  printMenu();

  option = getchar();

  switch (option)
  {
  case '1':
    sub_menu();
    break;

  case '2':
    get_reference(&internal_temperature, &reference_temperature);
    break;

  case '3':
    finish();
    break;

  default:
    system("clear");
    printf("Opção errada ou não desenvolvida, escolha novamente...\n");
    sleep(1);
    menu();
    break;
  }
}

void init()
{

  initUART();
  lcd_init();
  csvCreation();

  int i = bme280Init(CHANNEL_BME280, ADDRESS_BME280);
  if (i != 0)
  {
    printf("Erro to open\n");
  }

  pid_configure_consts(KP, KI, KD);
  wiringPiSetup();
  turnOFFFAN();
  turnOFFResistor();
}

int main(int argc, char const *argv[])
{
  signal(SIGINT, menu);
  init();
  menu();

  while (1)
  {
    sleep(1);
    getInformationModbus(&internal_temperature, &reference_temperature, ref_manual);

    getInformationBME280(&temperature_external);

    float temperatureExt = (float)temperature_external / 100;

    printInScreen(&temperatureExt, &internal_temperature, &reference_temperature);

    pid_update_reference(reference_temperature);

    control_signal = pid_control(internal_temperature);

    printLog(
        internal_temperature,
        temperatureExt,
        reference_temperature,
        control_signal);

    controlFanResistorPWM((int)control_signal);
  }

  finish();
  return 0;
}
