## Trabalho 1 Fundamentos de Sistemas Embarcados

### 1) Dados do Aluno:

- **Nome:** Ésio Gustavo Pereira Freitas
- **Matricula:** 1710033066

### 2) bibliotecas usadas

- **bme280**
- **wiringPiI2C**
- **wiringPi**
- **softPwm**

### 3) Instruções para executar

```
$ make

# entre na pasta bin

$ ./bin
```

### 4) Funcionalidades

- O usuário do programa pode escolher uma entre as três opções presentes:

  - **opção 1:** O usuário usa a temperatura do potenciômetro para interagir com o a ventoinha, sensores e resistores.

    - **opção 1.1:** O usuário usa on-off.
    - **opção 1.2:** PID.
    - **opção 1.3:** Chave externa.

  - **opção 2:** O usuário entra com uma temperatura de referência para que o programa comece interagir.

  - **opção 3:** O usuário sai do programa.

### 5) Experimento Executado:

- Para executar o experimento, foi feito medidas onde os dados durante a execução foram salvos em arquivos csv. Após a geração dos arquivos, foram plotado gráficos com as medidas.

![medicao 1](medidas/medicao.jpg)

### 6) Referências

[PID - Wikipedia](https://pt.wikipedia.org/wiki/Controlador_proporcional_integral_derivativo)  
[Driver da Bosh para o sensor BME280](https://github.com/BoschSensortec/BME280_driver)  
[Biblioteca BCM2835 - GPIO](http://www.airspayce.com/mikem/bcm2835/)  
[Controle do LCD 16x2 em C](http://www.bristolwatch.com/rpi/i2clcd.htm)  
[Biblioteca WiringPi GPIO](http://wiringpi.com)  
[PWM via WiringPi](https://www.electronicwings.com/raspberry-pi/raspberry-pi-pwm-generation-using-python-and-c)
